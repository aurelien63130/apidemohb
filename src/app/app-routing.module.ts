import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListingComponent} from "./components/listing/listing.component";
import {AjoutComponent} from "./components/ajout/ajout.component";
import {DetailComponent} from "./components/detail/detail.component";
import {EditComponent} from "./components/edit/edit.component";
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import {AuthGuardGuard} from "./guars/auth-guard.guard";

const routes: Routes = [
  {path: '', component: ListingComponent, canActivate: [AuthGuardGuard]},
  {path: 'ajout', component: AjoutComponent, canActivate: [AuthGuardGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: ':id', component: DetailComponent, canActivate: [AuthGuardGuard]},
  {path: 'edit/:id', component: EditComponent, canActivate: [AuthGuardGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
