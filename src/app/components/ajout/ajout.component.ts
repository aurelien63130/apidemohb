import { Component, OnInit } from '@angular/core';
import {Article} from "../../models/article";
import {ArticleService} from "../../services/article.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-ajout',
  templateUrl: './ajout.component.html',
  styleUrls: ['./ajout.component.css']
})
export class AjoutComponent implements OnInit {

  articleForm: Article = new Article();

  constructor(private articleService: ArticleService, private router: Router) { }

  ngOnInit(): void {
  }

  submitForm(){
    this.articleService.add(this.articleForm).subscribe(data => {
      this.router.navigate(['/']);
    });

  }

}
