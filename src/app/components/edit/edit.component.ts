import { Component, OnInit } from '@angular/core';
import {Article} from "../../models/article";
import {ArticleService} from "../../services/article.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  article!: Article;

 constructor(private articleService: ArticleService,
             private activatedRoute: ActivatedRoute,
             private router: Router) { }

  ngOnInit(): void {
    let id = parseInt(<string>this.activatedRoute.snapshot.paramMap.get('id'));

   this.articleService.getOne(id).subscribe(data => {
     this.article = data;
   })
  }

  submitForm(){
   this.articleService.update(this.article).subscribe(data => {
     this.router.navigate(["/"]);
   })
  }
}
