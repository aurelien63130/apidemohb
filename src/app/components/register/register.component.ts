import { Component, OnInit } from '@angular/core';
import {User} from "../../models/user";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: User = new User();

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  registerUser(): void{
    this.authService.register(this.userForm).subscribe(data => {
      this.router.navigate(['/login']);
    })

  }

}
