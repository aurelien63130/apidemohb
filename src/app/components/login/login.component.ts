import { Component, OnInit } from '@angular/core';
import {User} from "../../models/user";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm = new User();

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  connectUser(): void{
    this.authService.signIn(this.userForm);
  }
}
