import { Component, OnInit } from '@angular/core';
import {ArticleService} from "../../services/article.service";
import {Article} from "../../models/article";
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  articles!: Article[];
  icon = faEye;
  type = 'password'
  constructor(private articleService: ArticleService, private authService: AuthService) { }

  ngOnInit(): void {
    this.articleService.getAll().subscribe(data => {
        this.articles = data;
    });
  }

  removeArticle(id: number){
    this.articleService.remove(id).subscribe(data => {
      this.ngOnInit();
    });


  }

  changeFormType(){
    if(this.icon == faEye){
      this.type = 'text';
      this.icon = faEyeSlash;
    } else {
      this.type = 'password';
      this.icon = faEye;
    }

  }

  logout():void {
    this.authService.doLogout();
  }

}
