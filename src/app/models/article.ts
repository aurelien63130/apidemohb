export class Article {
  id?: number;
  title?: string;
  contenu?: string;
  image?: string;
}
