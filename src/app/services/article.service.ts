import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Article} from "../models/article";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  apiUrl = environment.api + 'api/articles';

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Article[]>{
    return this.httpClient.get<Article[]>(this.apiUrl+ '.json');
  }

  add(article: Article): Observable<Article>{
    return this.httpClient.post<Article>(this.apiUrl+ '.json', article);
  }

  remove(id: number): Observable<Article>{
    return this.httpClient.delete<Article>(this.apiUrl+"/"+id+ '.json');
  }

  getOne(id: number): Observable<Article>{
    return this.httpClient.get(this.apiUrl+"/"+id + '.json');
  }

  update(article: Article): Observable<Article> {
    return this.httpClient.put<Article>(this.apiUrl + '/'+article.id + '.json', article);
  }
}
